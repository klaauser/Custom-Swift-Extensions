//
//  UIView-Rotate.swift
//  Created by Kyle Laauser on 7/3/18.
//  Swift 4.0
//

import UIKit

extension UIView {
    private static let kRotationAnimationKey = "rotationanimationkey"
    private static let kRotationAnimationKeyEnding = "rotationanimationkeyending"
    
    func rotate(duration: Double = 5) {
        if layer.animation(forKey: UIView.kRotationAnimationKey) == nil {
            if layer.animation(forKey: UIView.kRotationAnimationKeyEnding) != nil {
                layer.removeAnimation(forKey: UIView.kRotationAnimationKeyEnding)
            }
            
            let rotationAnimation = CABasicAnimation(keyPath: "transform.rotation")
            
            rotationAnimation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
            rotationAnimation.fillMode = kCAFillModeForwards
            rotationAnimation.isRemovedOnCompletion = false
            rotationAnimation.fromValue = 0
            rotationAnimation.toValue = CGFloat.pi * 2
            rotationAnimation.duration = duration
            rotationAnimation.repeatCount = Float.infinity
            
            layer.add(rotationAnimation, forKey: UIView.kRotationAnimationKey)
        }
    }
    
    func stopRotating(duration: Double = 5) {
        if layer.animation(forKey: UIView.kRotationAnimationKey) != nil && layer.animation(forKey: UIView.kRotationAnimationKeyEnding) == nil {
            let rotationAnimation = CABasicAnimation(keyPath: "transform.rotation")
            
            rotationAnimation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseOut)
            rotationAnimation.fillMode = kCAFillModeForwards
            rotationAnimation.isRemovedOnCompletion = false
            rotationAnimation.fromValue = layer.presentation()?.value(forKeyPath: "transform.rotation")
            rotationAnimation.toValue = 0 //CGFloat.pi * 2
            rotationAnimation.duration = 5
            rotationAnimation.repeatCount = 1
            
            layer.add(rotationAnimation, forKey: UIView.kRotationAnimationKeyEnding)
            layer.removeAnimation(forKey: UIView.kRotationAnimationKey)
        }
    }
}
