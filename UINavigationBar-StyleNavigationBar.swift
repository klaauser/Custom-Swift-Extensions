//
//  UIFont-ScratchdFonts.swift
//  Created by Kyle Laauser on 7/3/18.
//  Swift 4.0
//

import UIKit

extension UINavigationBar {
    static func styleNavigationBar(largeBarFont: UIFont, smallBarFont: UIFont, largeBarTextColor: UIColor, smallBarTextColor: UIColor, barTintColor: UIColor, tintColor: UIColor){
        let navigationBarAppearance = UINavigationBar.appearance()
        navigationBarAppearance.largeTitleTextAttributes = [
            NSAttributedStringKey.font: largeBarFont,
            NSAttributedStringKey.foregroundColor: largeBarTextColor]
        
        navigationBarAppearance.titleTextAttributes = [
            NSAttributedStringKey.font: smallBarFont,
            NSAttributedStringKey.foregroundColor: smallBarTextColor]
        
        navigationBarAppearance.tintColor = tintColor
        navigationBarAppearance.barTintColor = barTintColor
        navigationBarAppearance.isTranslucent = false
    }
}
