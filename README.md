This git repository contains non-proprietary Swift extensions used in various Spacecat Apps.  
Source and/or Inspiration has been documented where I remembered to.  
Please email spacecat.apps@gmail.com with any questions, comments, or suggestions.
