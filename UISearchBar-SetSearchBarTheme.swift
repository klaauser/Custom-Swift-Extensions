//
//  UIFont-ScratchdFonts.swift
//  Created by Kyle Laauser on 7/3/18.
//  Swift 4.0
//

import UIKit

extension UISearchBar {
    func setSearchBarTheme(_ cornerRadius: CGFloat, clipsToBounds:Bool, backgroundColor: UIColor, textColor: UIColor, tintColor:UIColor, searchButton: UIImage, clearButton: UIImage, placeholder: String){
        
        self.setImage(searchButton, for: UISearchBarIcon.search, state: .normal)
        self.placeholder = placeholder
        self.tintColor = tintColor
        
        UITextField.appearance(whenContainedInInstancesOf: [UISearchBar.self]).defaultTextAttributes = [NSAttributedStringKey.foregroundColor.rawValue: textColor]
        
        if let searchField = (value(forKey: "searchField") as? UITextField) {
            
            if let searchFieldBackground = searchField.subviews.first {
                searchFieldBackground.backgroundColor = backgroundColor
                searchFieldBackground.layer.cornerRadius = cornerRadius
                searchFieldBackground.clipsToBounds = clipsToBounds
            }
            
            if let clear = searchField.value(forKey: "_clearButton") as? UIButton {
                clear.setImage(clearButton, for: .normal)
            }
        }
    }
}
