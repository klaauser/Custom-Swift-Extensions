//
//  UIFont-ScratchdFonts.swift
//  Created by Kyle Laauser on 7/3/18.
//  Swift 4.0
//

import UIKit

extension UINavigationController {
    static func setNavigationBarTitleImageView(navigationController: UINavigationController, image: UIImage, x: CGFloat, y: CGFloat, height: CGFloat, width: CGFloat, contentMode: UIViewContentMode, center: Bool) {
        
        let navigationBarTitleImageView: UIImageView = UIImageView(frame: CGRect(x: x, y: y, width: navigationController.navigationBar.frame.width, height: navigationController.navigationBar.frame.height))
        
        navigationBarTitleImageView.image = image
        navigationBarTitleImageView.contentMode = contentMode
        
        if center == true {
            navigationBarTitleImageView.center = navigationController.navigationBar.center
        }
        
        navigationController.navigationItem.titleView = navigationBarTitleImageView
    }
}
