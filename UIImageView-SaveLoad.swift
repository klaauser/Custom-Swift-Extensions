//
//  UIImageView-SaveLoad.swift
//  Created by Kyle Laauser on 7/3/18.
//  Swift 4.0
//

import UIKit

extension UIImageView {
    func save(url: URL, name: String, contentMode: UIViewContentMode = .scaleAspectFit) {
        let imageView: UIImageView = UIImageView()
        imageView.downloadedFrom(url: url, contentMode: contentMode)
        
        let image = imageView.image!
        
        let imagePath: String = "\(NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0])/\(name).png"
        
        let imageFileUrl: URL = URL(fileURLWithPath: imagePath)
        
        try? UIImagePNGRepresentation(image)?.write(to: imageFileUrl)
    }
    
    func save(name: String, image: UIImage) {
        let imagePath: String = "\(NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0])/\(name).png"
        
        let imageFileUrl: URL = URL(fileURLWithPath: imagePath)
        
        try? UIImagePNGRepresentation(image)?.write(to: imageFileUrl)
    }
    
    func save(name: String, imageView: UIImageView) {
        let image = imageView.image!
        
        let imagePath: String = "\(NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0])/\(name).png"
        
        let imageFileUrl: URL = URL(fileURLWithPath: imagePath)
        
        try? UIImagePNGRepresentation(image)?.write(to: imageFileUrl)
    }
    
    func load(name: String) -> UIImage {
        let imagePath: String = "\(NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0])/\(name).png"
        let imageUrl: URL = URL(fileURLWithPath: imagePath)
        
        var image: UIImage {
            var foundImage = UIImage()
            if FileManager.default.fileExists(atPath: imagePath),
                let imageData: Data = try? Data(contentsOf: imageUrl),
                let image = UIImage(data: imageData, scale: UIScreen.main.scale) {
                foundImage = image
            }
            return foundImage
        }
        
        return image
    }
}
