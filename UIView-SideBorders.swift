//
//  CALayer-SideBorders.swift
//  Created by Kyle Laauser on 7/3/18.
//  Swift 4.0
//
//  INSPIRATION/SOURCE:https://stackoverflow.com/questions/17355280/how-to-add-a-border-just-on-the-top-side-of-a-uiview

import UIKit

extension UIView {
    
    func addTopBorderWithColor(color: UIColor, width: CGFloat) {
        let border = UIView()
        
        border.backgroundColor = color
        border.autoresizingMask = [.flexibleWidth, .flexibleBottomMargin]
        border.frame = CGRect(x: 0, y: 0, width: self.frame.width, height: width)
        
        self.addSubview(border)
    }
    
    func addBottomBorderWithColor(color: UIColor, width: CGFloat) {
        let border = UIView()
        
        border.backgroundColor = color
        border.autoresizingMask = [.flexibleWidth, .flexibleTopMargin]
        border.frame = CGRect(x: 0, y: self.frame.height - width, width: self.frame.width, height: width)
        
        self.addSubview(border)
    }
    
    func addLeftBorderWithColor(color: UIColor, width: CGFloat) {
        let border = UIView()
        
        border.backgroundColor = color
        border.autoresizingMask = [.flexibleHeight, .flexibleRightMargin]
        border.frame = CGRect(x: 0, y: 0, width: width, height: self.frame.height)
        
        self.addSubview(border)
    }
    
    func addRightBorderWithColor(color: UIColor, width: CGFloat) {
        let border = UIView()
        
        border.backgroundColor = color
        border.autoresizingMask = [.flexibleHeight, .flexibleLeftMargin]
        border.frame = CGRect(x: self.frame.width - width, y: 0, width: width, height: self.frame.height)
        
        self.addSubview(border)
    }
    
    // SOURCE: https://stackoverflow.com/questions/10167266/how-to-set-cornerradius-for-only-top-left-and-top-right-corner-of-a-uiview
    func roundCorners(_ corners: UIRectCorner, radius: CGFloat) {
        let path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        self.layer.mask = mask
    }
}
