//
//  UIFont-ScratchdFonts.swift
//  Created by Kyle Laauser on 7/3/18.
//  Swift 4.0
//

import UIKit

extension UISearchController {
    func configureSearchController(searchResultsUpdater: UISearchResultsUpdating, obscuresBackgroundDuringPresentation: Bool){
        self.searchResultsUpdater = searchResultsUpdater
        self.obscuresBackgroundDuringPresentation = obscuresBackgroundDuringPresentation
    }
}
