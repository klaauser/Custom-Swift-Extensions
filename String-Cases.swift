//
//  UIFont-ScratchdFonts.swift
//  Created by Kyle Laauser on 7/3/18.
//  Swift 4.0
//
//  INSPIRATION/SOURCE: https://stackoverflow.com/questions/26306326/swift-apply-uppercasestring-to-only-the-first-letter-of-a-string

import Foundation

extension String {
    func capitalizingFirstLetter() -> String {
        return prefix(1).uppercased() + dropFirst()
    }
    
    func camelCased() -> String {
        let lineComponents = components(separatedBy: .whitespaces)
        var camelCasedComponents: [String]!
        for component in lineComponents {
            if component.isEmpty == false {
                camelCasedComponents.append(component.prefix(1).uppercased() + dropFirst())
            }
        }
        
        return camelCasedComponents.filter { !$0.isEmpty }.joined(separator: " ")
    }
    
    func lowercasingFirstLetter() -> String {
        return prefix(1).lowercased() + dropFirst()
    }
    
    mutating func capitalizeFirstLetter() {
        self = self.capitalizingFirstLetter()
    }
}
